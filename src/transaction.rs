use serde::{Serialize,Deserialize};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters};
use std::slice;
use bincode::{serialize, deserialize};
use rand::{thread_rng, Rng};
use rand::distributions::Alphanumeric;
use crate::crypto::hash::{Hashable, H256, H160, Hashable160};
use ring::{digest};
use crate::crypto::key_pair;


#[derive(Serialize, Deserialize, Debug, Default, Clone, PartialEq)]
pub struct input {
    pub prevT: Option<H256>,
    pub index: u32,
    // pub signature: bool,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone, PartialEq)]
pub struct output {
    pub reciAddress: H160,
    pub value:u32,
    // pub signature: bool,
}

#[derive(Serialize, Deserialize, Debug, Default, Clone, PartialEq)]
pub struct signedT {
    pub transaction: Transaction,
    // pub signature: bool,
    pub signature: Option<Vec<u8>>,
    pub public_key: Option<Vec<u8>>,
}


#[derive(Serialize, Deserialize, Debug, Default, Clone, PartialEq)]
pub struct Transaction {
    pub input: Option<Vec<input>>,
    pub output: Option<Vec<output>>,
}

impl Hashable for Transaction {
    fn hash(&self) -> H256 {
        let temp = digest::digest(&digest::SHA256, &serialize(&self).unwrap());
        <H256>::from(temp)
    }
}

impl Hashable for signedT {
    fn hash(&self) -> H256 {
        (self.transaction).hash()
    }
}

impl signedT{
    pub fn sign(mut self, sig: Signature, key:  &<Ed25519KeyPair as KeyPair>::PublicKey) ->Self{
        let a = sig.as_ref();
        let b: Vec<u8> = a.iter().cloned().collect();
        // let mut c = &<Ed25519KeyPair as KeyPair>::public_key(key);
        let d: Vec<u8> = key.as_ref().iter().cloned().collect();
        self.signature = Some(b);
        self.public_key = Some(d);
        return self;
    }
}

/// Create digital signature of a transaction
pub fn sign(t: &Transaction, key: &Ed25519KeyPair) -> Signature {
    //unimplemented!()
    let t_s = serialize(&t).unwrap();
    let sig = key.sign(&t_s);
    // let sigb = bincode::serialize(&sig).unwrap();
    let a = sig.as_ref();
    let b: Vec<u8> = a.iter().cloned().collect();
    let mut c = key.public_key();
    let d: Vec<u8> = c.as_ref().iter().cloned().collect();
    // t.signature = Some(b);
    // t.sign(b, d);
    return sig;
}

/// Verify digital signature of a transaction, using public key instead of secret key
pub fn verify(t: &Transaction, public_key: &<Ed25519KeyPair as KeyPair>::PublicKey, signature: &Signature) -> bool {
    //unimplemented!()
    let t_s = serialize(&t).unwrap();
    // let ret = public_key.verify(slice, signature);
    let peer_public_key_bytes = public_key.as_ref();
    let peer_public_key = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, peer_public_key_bytes);
    // signature::verify()
    let ret = peer_public_key.verify(&t_s, signature.as_ref());
    // try!(peer_public_key.verify(&t_s, signature.as_ref()));
    if ret == Ok(()) { return true;}
    else {return false;}

}

pub fn generate_random_t() -> signedT {
        // Default::default()
        //unimplemented!()
        let rand_u8 = digest::digest(&digest::SHA256,"442cabd17e40d95ac0932d977c0759397b9db4d93c4d62c368b95419db574db0".as_bytes());
        let diff_rand = <H160>::from(rand_u8);
        let mut rng = rand::thread_rng();
        let n2:u32 = rng.gen();
        let input = input{
            prevT : None,
            index: n2,
        };

        let output = output{
            reciAddress: diff_rand,
            value: n2,
        };

        // let signature = None;
        // signature = signature.to_string();
        let mut vec1 = Vec::new();
        let mut vec2 = Vec::new();
        vec1.push(input);
        vec2.push(output);
        let key = key_pair::random();
        let ret = Transaction{input: Some(vec1), output: Some(vec2)};
        let sig = sign(& ret, &key);
        let a = sig.as_ref();
        let b: Vec<u8> = a.iter().cloned().collect(); 
        let mut c = key.public_key();
        let d: Vec<u8> = c.as_ref().iter().cloned().collect();
        let signedT = signedT{transaction: ret, signature: Some(b), public_key: Some(d)};
        // let key = key_pair::random();

        return signedT;

}

#[cfg(any(test, test_utilities))]
mod tests {
    use super::*;
    use crate::crypto::key_pair;

    pub fn generate_random_transaction() -> Transaction {
        // Default::default()
        //unimplemented!()
        let rand_u8 = digest::digest(&digest::SHA256,"442cabd17e40d95ac0932d977c0759397b9db4d93c4d62c368b95419db574db0".as_bytes());
        let diff_rand = <H160>::from(rand_u8);
        let mut rng = rand::thread_rng();
        let n2:u32 = rng.gen();
        let input = input{
            prevT : None,
            index: n2,
        };

        let output = output{
            reciAddress: diff_rand,
            value: n2,
        };

        // let signature = None;
        // signature = signature.to_string();
        let mut vec1 = Vec::new();
        let mut vec2 = Vec::new();
        vec1.push(input);
        vec2.push(output);
        let key = key_pair::random();
        let ret = Transaction{input: Some(vec1), output: Some(vec2)};
        return ret;
        // let sig = sign(& ret, &key);
        // let a = sig.as_ref();
        // let b: Vec<u8> = a.iter().cloned().collect(); 
        // let mut c = key.public_key();
        // let d: Vec<u8> = c.as_ref().iter().cloned().collect();
        // let signedT = signedT{transaction: ret, signature: Some(b), public_key: Some(d)};
        // // let key = key_pair::random();

        // return ret;

    }

    #[test]
    fn sign_verify() {
        let t = generate_random_transaction();
        let key = key_pair::random(); 
        let signature = sign(&t, &key);
        assert!(verify(&t, &(key.public_key()), &signature));
    }
}
