use super::message::Message;
use super::peer;
use crate::network::server::Handle as ServerHandle;
use crossbeam::channel;
use log::{debug, warn};
use crate::crypto::hash::{Hashable, H256, H160, Hashable160};
use ring::{digest};
use std::sync::{Arc, Mutex};
use crate::blockchain::{Blockchain};
use crate::block::{Block,Header};
use std::thread;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use std::collections::HashMap;
use crate::transaction::{Transaction, verify, signedT};
use ring::signature::{Ed25519KeyPair, Signature, KeyPair, VerificationAlgorithm, EdDSAParameters, };
use bincode::{serialize, deserialize};

#[derive(Clone)]
pub struct Context {
    msg_chan: channel::Receiver<(Vec<u8>, peer::Handle)>,
    num_worker: usize,
    server: ServerHandle,
    blockchain: Arc<Mutex<Blockchain>>,
    Mempool: Arc<Mutex<HashMap<H256, signedT>>>,
}

pub fn new(
    num_worker: usize,
    msg_src: channel::Receiver<(Vec<u8>, peer::Handle)>,
    server: &ServerHandle,
    blockchain: &Arc<Mutex<Blockchain>>,
    Mempool: &Arc<Mutex<HashMap<H256, signedT>>>
) -> Context {
    // let block = Blockchain::new();
    // let block_m = Arc::new(Mutex::new(block));
    Context {
        msg_chan: msg_src,
        num_worker,
        server: server.clone(),
        blockchain: Arc::clone(blockchain),
        Mempool: Arc::clone(Mempool),
    }
}

pub fn checks(t: &signedT)->bool{
    let k = t.public_key.as_ref().unwrap();
    let ke = &k[..];
    let key = Ed25519KeyPair::from_pkcs8_maybe_unchecked(ke).unwrap();
    let key1 = key.public_key();
    let s = t.signature.as_ref().unwrap();
    let mut si = &s[..];
    let t_s = serialize(&t.transaction).unwrap();
    // let si = **si;
    // let sig: Signature = (si).TryInto();
    let peer_public_key = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, key1);
    let ret = peer_public_key.verify(&t_s, si);
    if ret == Ok(()) { return true;}
    else {return false;}  
}

impl Context {
    pub fn start(self) {
        let num_worker = self.num_worker;
        for i in 0..num_worker {
            let cloned = self.clone();
            thread::spawn(move || {
                cloned.worker_loop();
                warn!("Worker thread {} exited", i);
            });
        }
    }

    fn worker_loop(&self) {
        loop {
            let msg = self.msg_chan.recv().unwrap();
            let (msg, peer) = msg;
            let msg: Message = bincode::deserialize(&msg).unwrap();
            match msg {

                Message::NewTransactionHashes(nonce) => {
                    // debug!("Pong: {:?}", nonce);
                    if nonce.len() != 0{
                        let mut vec = Vec::new();
                        for i in nonce.iter(){
                            if self.Mempool.lock().unwrap().get(&i).is_none(){

                                vec.push(*i);
                            }
                        }
                        if vec.len() != 0{
                            peer.write(Message::GetTransactions(vec));
                        }
                    }
                }

                Message::GetTransactions(blk) => {
                    // debug!("Pong: {:?}", nonce);
                    let Mempool = self.Mempool.lock().unwrap();
                    let mut nonce = blk.clone();
                    // if nonce.len() != 0{
                    let mut vec = Vec::new();
                    for i in nonce.iter(){
                        if !Mempool.get(&i).is_none(){
                            // let temp = self.blockchain.lock().unwrap().map.get(&i).unwrap();
                            // peer.write(Message::GetBlocks(nonce));
                            vec.push(Mempool.get(&i).unwrap().clone());
                        }
                    }
                    if vec.len() != 0{
                        // println!("We got here: {:?}", vec.len());
                        peer.write(Message::Transactions(vec));
                    }

                }

                Message::Transactions(blk) => {
                    // debug!("Pong: {:?}", nonce);
                    let mut Mempool = self.Mempool.lock().unwrap();
                    let mut nonce = blk.clone();
                    // let mut vec = Vec::new();
                    // let mut vec1 = Vec::new();
                    // println!("{:?}",blockchain.Orphan.capacity());
                    for i in nonce.iter(){
                        // let h = 
                        if Mempool.get(&i.hash()).is_none(){
                            // verify(i.)
                            let k = i.public_key.as_ref().unwrap();
                            let ke = &k[..];
                            let key = Ed25519KeyPair::from_pkcs8_maybe_unchecked(ke).unwrap();
                            let key1 = key.public_key();
                            let s = i.signature.as_ref().unwrap();
                            let mut si = &s[..];
                            let t_s = serialize(&i.transaction).unwrap();
                            // let si = **si;
                            // let sig: Signature = (si).TryInto();
                            let peer_public_key = ring::signature::UnparsedPublicKey::new(&ring::signature::ED25519, key1);
                            let ret = peer_public_key.verify(&t_s, si);
                            if ret == Ok(()){
                                Mempool.insert(i.hash(), i.clone());
                            }

                        }
                    }

                    // peer.write(Message::GetBlocks(vec1));
                    // println!("i am here");
                    // self.server.broadcast(Message::NewBlockHashes(vec));

                }



                Message::Ping(nonce) => {
                    debug!("Ping: {}", nonce);
                    peer.write(Message::Pong(nonce.to_string()));
                }

                Message::Pong(nonce) => {
                    debug!("Pong: {}", nonce);
                }

                Message::NewBlockHashes(nonce) =>{
                    // debug!("NewBlockHashes: {:?}", nonce);
                    if nonce.len() != 0{
                        let mut vec = Vec::new();
                        for i in nonce.iter(){
                            if self.blockchain.lock().unwrap().map.get(&i).is_none(){
                                vec.push(*i);
                                // peer.write(Message::GetBlocks(nonce));
                            }
                        }
                        if vec.len() != 0{
                            // println!("We got here first: {:?}", vec.len());
                            peer.write(Message::GetBlocks(vec));
                        }
                    }
                }

                Message::GetBlocks(blk) => {
                    // debug!("GetBlocks: {:?}", blk);
                    let blockchain = self.blockchain.lock().unwrap();
                    let mut nonce = blk.clone();
                    // if nonce.len() != 0{
                        let mut vec = Vec::new();
                        for i in nonce.iter(){
                            if !blockchain.map.get(&i).is_none(){
                                // let temp = self.blockchain.lock().unwrap().map.get(&i).unwrap();
                                // peer.write(Message::GetBlocks(nonce));
                                vec.push(blockchain.map.get(&i).unwrap().clone());
                            }
                        }
                        if vec.len() != 0{
                            // println!("We got here: {:?}", vec.len());
                            peer.write(Message::Blocks(vec));
                        }
                    // }
                }

                Message::Blocks(blk) => {
                    // debug!("Blocks: {:?}", blk);
                    let mut blockchain = self.blockchain.lock().unwrap();
                    let mut nonce = blk.clone();
                    let mut vec = Vec::new();
                    let mut vec1 = Vec::new();
                    let mut latency;
                    // let mut Mempool = self.Mempool.lock().unwrap();
                    println!("{:?}",blockchain.Orphan.capacity());
                    for i in nonce.iter_mut(){
                        // let h = 
                        if blockchain.map.get(&i.hash()).is_none(){
                            // println!("i am here parent");

                            // println!("i am here");
                            let parent = i.header.clone().unwrap().parant.unwrap();
                            if  blockchain.map.get(&parent).is_some(){
                                let diff = blockchain.map.get(&parent).unwrap().header.as_ref().unwrap().difficulty;
                                let in_ms:u64;
                                                               
                                if i.hash() <=diff{
                                    vec.push(i.hash());
                                    // let mut content = &mut i.content;
                                    let mut che = true;
                                    let mut cou = 15;
                                    for k in i.content.iter(){
                                        che = checks(k);
                                        cou -= 1;
                                        if !che{
                                            break;
                                        }
                                    }
                                    
                                    if che{
                                        let mem = self.Mempool.lock().unwrap().clone();
                                        for (key, val) in mem.iter(){
                                            if cou == 0 { break;}
                                            cou -= 1;
                                            i.content.push(val.clone());
                                            self.Mempool.lock().unwrap().remove(&key);

                                        }

                                        blockchain.insert(&i);
                                    } 
                                    match SystemTime::now().duration_since(UNIX_EPOCH) {
                                        Ok(n) => in_ms = n.as_secs() * 1000 + n.subsec_nanos() as u64 / 1000000,
                                        Err(_) => panic!("SystemTime before UNIX EPOCH!"),
                                    }
                                    latency = in_ms - i.header.clone().unwrap().timestamp;
                                    println!("latency is {:?}", latency);

                                    let mut child = blockchain.Orphan.get(&i.hash());
                                    if blockchain.Orphan.get(&i.hash()).is_some(){
                                        println!("i am here for parent");
                                        let mut check = blockchain.Orphan.get(&i.hash()).unwrap().clone();
                                        for j in check.iter(){
                                            // println!("i am here parent1");
                                            let content = &j.content;
                                            che = true;
                                            
                                                for k in content.iter(){
                                                    che = checks(k);
                                                    if !che{
                                                        break;
                                                    }
                                                }
                                            
                                            if che{blockchain.insert(&j);} 
                                            // blockchain.insert(&j);
                                        }
                                        blockchain.Orphan.remove(&i.hash());
                                    }
                                }
                            }
                            else{
                                // blockchain.Orphan.get_mut(&i.hash());
                                println!("i am here no parent");
                                if blockchain.Orphan.get(&i.hash()).is_none(){
                                    // println!("i am here");
                                    let mut vec3 = Vec::new();
                                    vec3.push(i.clone());
                                    blockchain.Orphan.insert(*parent, vec3);
                                    // println!("i am here");
                                }
                                else{
                                    println!("i am here1");
                                    let mut k = blockchain.Orphan.get_mut(&i.hash()).unwrap();
                                    k.push(i.clone());
                                }
                                vec1.push(*parent);
                            }
                        }
                    }

                    peer.write(Message::GetBlocks(vec1));
                    println!("i am here");
                    self.server.broadcast(Message::NewBlockHashes(vec));
                }
            }
        }
    }
}
